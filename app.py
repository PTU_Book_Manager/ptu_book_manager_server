from flask import Flask
from flask_restful import Resource, Api
from flask_restful import reqparse
from flaskext.mysql import MySQL
import data as dt

app = Flask(__name__)
api = Api(app)

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'booster1'
app.config['MYSQL_DATABASE_DB'] = 'PTU'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

class Createuser(Resource):
    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('hakbun', type=str)
            parser.add_argument('email', type=str)
            parser.add_argument('username', type=str)
            parser.add_argument('password', type=str)
            args = parser.parse_args()
            
            _userHakbun = args['hakbun']
            _userEmail = args['email']
            _userName = args['username']
            _userPassword = args['password']

            
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_create_user', (_userHakbun,_userEmail, _userName, _userPassword))
            data = cursor.fetchall()

            if len(data) is 0:
                conn.commit()
                return {'StatusCode': '200', 'Message': 'User creation success'}
            else:
                return {'StatusCode': '1000', 'Message': str(data[0])}
        except Exception as e:
            return {'error': str(e)}

class Loginuser(Resource):
    def post(self):
        try:
            lg = dt.User()
            parser = reqparse.RequestParser()
            parser.add_argument('hakbun', type=str)
            parser.add_argument('password', type=str)
            args = parser.parse_args()

            _userHakbun = args['hakbun']
            _userPassword = args['password']
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_validateLogin', (_userHakbun,))
            data = cursor.fetchall()

            lg.loginUser(_userHakbun,_userPassword)

            if len(data) > 0:
                if str(data[0][3]) == _userPassword:
                    return {'StatusCode': '200', 'Message': 'User creation success'}
                else:
                    return {'StatusCode': '1000', 'Message': str(data[0])}
            else:
                return {'StatusCode': '1000', 'Message': str(data[0])}
        except Exception as e:
            return {'status' : 'fail', 'reason' : str(e)}

class RandomBook(Resource):
    def post(self):
        try:
            d = dt.Book()
            d.RandomBook()
        except Exception as e:
            return {'status' : 'fail', 'reason' : str(e)}


api.add_resource(Createuser, '/user/create')
api.add_resource(Loginuser, '/user/login')
api.add_resource(RandomBook,'/book/random')

if __name__ == '__main__':
    app.run(debug=True)

