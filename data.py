import json
from time import gmtime, strftime
import barcode as bc
import codecs
import random

class User:
    def __init__(self):
        with open('user.json') as f:
                data = json.load(f)
        self.data = data

    def CreateUser(self, hakbun, username, pw, email):
        userdata = {"hakbun": hakbun, "user_name":username, "password":pw, "email":email, "books" : []}
        self.data.append(userdata)

        with codecs.open('user.json', 'w', encoding='utf-8') as f:
            json.dump(self.data, f, ensure_ascii=False)
        return {'status' : 'success'}

    def loginUser(self, hakbun,password):
        d = {}
        for i in range(0,len(self.data)):
            if self.data[i]["hakbun"] == int(hakbun) and self.data[i]["password"] == password:
                d = self.data[i]
                return d
        print(d)
        return d

class Book:
    def __init__(self):
        with open('books.json') as f:
                data = json.load(f)
        self.data = data
    
    def CreateBook(self, isbn):
        bookdata = bc.parsing(isbn)

        for i in range(0,len(self.data)):
            if bookdata["title"] == self.data[i]["title"]:
                self.data[i]["count"] += 1
                json.loads(self.data[i]["possible"])
            else:
                userdata = {"possible":"['False']","count":1, "image": bookdata['image'], "price":bookdata['price'], "pubdate":bookdata['pubdate'], "publisher":bookdata["publisher"], "author":bookdata["author"], "title":bookdata["title"]}
                self.data.append(userdata)

        with codecs.open('books.json', 'w', encoding='utf-8') as f:
            json.dump(self.data, f, ensure_ascii=False)

        return {'status' : 'success'}



    def BorrowBook(self, user_id, isbn):
        user = User()
        bookdata = bc.parsing(isbn)
        current_time = strftime("%Y-%m-%d", gmtime())
        borrow_book_data = (bookdata['title'], current_time)
        
        for book in self.data:
            if book['title'] == bookdata['title']:
                if "False" in book["possible"]:
                    # 대여 표시로 바꿔주기
                    for i in range(0,len(book["possible"])):
                        if book["possible"][i] == "False":
                            book["possible"][i] = "True"
                            break
                    for users in user.data:
                        if users['user_name'] == user_id:
                            users['books'].append(borrow_book_data)
                    return {"status" : "success"}
                else:
                    return {"status" : "fail", "reason" : "현재 빌릴수 없는 책입니다."}

    
    def ReturnBook(self, user_id, isbn):
        user = User()
        bookdata = bc.parsing(isbn)
        try:
            for book in self.data:
                if book['title'] == bookdata['title']:
                    if "True" in book["possible"]:
                        for i in range(0,len(book["possible"])):
                            if book["possible"][i] == "True":
                                book["possible"][i] = "False"
                                break
                        for users in user.data:
                            if users['user_name'] == user_id:
                                for i in range(0,len(users['books'])):
                                    if users['books'][i][0] == bookdata['title']:
                                        del users['books'][i]
                                        break
                break
            return {'status':'success'}
        except Exception as e:
            return {'status':'fail', 'reason' : str(e)}

    def RandomBook(self):
        index = random.randrange(0,len(self.data))
        data = self.data
        return {'status':'success', 'data' : data[index]}