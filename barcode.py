import urllib.request
import os, ssl
from xml.etree.ElementTree import parse

if (not os.environ.get('PYTHONHTTPSVERIFY', '') and
    getattr(ssl, '_create_unverified_context', None)): 
    ssl._create_default_https_context = ssl._create_unverified_context


def parsing(isbn):
    client_id = "niB9_CXggiK8A3eGJZAp"
    client_secret = "bBP1MSFPxE"
    url = "https://openapi.naver.com/v1/search/book_adv.xml?d_isbn=" + str(isbn) # json 결과
    request = urllib.request.Request(url)
    request.add_header("X-Naver-Client-Id",client_id)
    request.add_header("X-Naver-Client-Secret",client_secret)
    response = urllib.request.urlopen(request)
    rescode = response.getcode()
    if(rescode==200):
        data = parse(response)
        return py_xml_proc(data)
    else:
        print("Error Code:" + rescode)
        
def split_url(string):
    s = string.find('?')
    return string[:s]
    
def py_xml_proc(data):
    b_data = {}
    for item in data.iterfind('channel/item'):
        b_data['title'] = item.findtext('title')
        b_data['author'] = item.findtext('author')
        b_data['publisher'] = item.findtext('publisher')
        b_data['pubdate'] = item.findtext('pubdate')
        b_data['price'] = item.findtext('price')
        b_data['image'] = split_url(item.findtext('image'))
    return b_data

